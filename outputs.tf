output "ip_address_external" {
    value = module.tf-yc-instance.ip_address_external
}
output "vpc_network" {
    value = module.tf-yc-network.vpc_network
}
output "network_zones" {
	value = module.tf-yc-network.network_zones
}
output "subnet_id_result" {
	value = module.tf-yc-instance.subnet_id_result
}
output "yandex_vpc_subnet" {
	value = module.tf-yc-network.yandex_vpc_subnet
}