data "yandex_vpc_network" "default" {
  name="default"
#  description = "Receive default name in network"
}
data "yandex_vpc_subnet" "subnet" {
  for_each  = toset(data.yandex_vpc_network.default.subnet_ids)
  subnet_id = "${each.value}"
#  description = "Receive subnet ids in string"

}
data "yandex_vpc_subnet" "default" {
  name = data.yandex_vpc_subnet.subnet[var.subnet_id].name
# description = "Output vpc subnet (incorrect working, output all value)"
}

