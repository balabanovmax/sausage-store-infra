variable "network_zones" {
  type = map(string) 
  default = { 
    ru-central1-a = "e9b9i3jkm7hdhu9rkteg" 
    ru-central1-b = "e2llfd56mjp1uj3mg2rf" 
    ru-central1-c = "b0cebar894suaoejt69j" 
  }
  description = "All ID network zone "
} 
variable "subnet_id" {}
