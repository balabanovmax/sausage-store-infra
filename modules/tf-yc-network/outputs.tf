output "vpc_network" {
	description = "Yandex.Cloud Network"
	value		= data.yandex_vpc_network.default
}
output "network_zones" {
	description = "Output network zones"
	value	= var.network_zones
}
output "yandex_vpc_subnet" {
	description = "Output vpc subnet (incorrect working, output all value)"
	value = data.yandex_vpc_subnet.default
}
