Модуль для сети инстанса ВМ
Используются переменные, задаются в 
./terraform/modules/tf-yc-network/variables.tf:


    variable "network_zones" {
      default = { 
      ru-central1-a = "e9b9i3jkm7hdhu9rkteg" 
      ru-central1-b = "e2llfd56mjp1uj3mg2rf" 
      ru-central1-c = "b0cebar894suaoejt69j" 
      }
    description = "All ID network zone "
    variable "zone" {
      default           = "ru-central1-b"
      description       = "Instance availability zone"
      }

Генерируется автоматически: в main.tf

    variable "subnet_id" {}
      lookup(module.tf-yc-network.network_zones, var.zone)


Используется провайдер:

    source  = "yandex-cloud/yandex"
    version = "=0.99.0"


Выводится:

    output "vpc_network" {
	    description = "Yandex.Cloud Network"
	    value		= data.yandex_vpc_network.default
    }    
    output "network_zones" {
	    description = "Output network zones"
	    value	= var.network_zones
    }
    output "yandex_vpc_subnet" {
	    description = "Output vpc subnet (incorrect working, output all value)"
	    value = data.yandex_vpc_subnet.default
    }
