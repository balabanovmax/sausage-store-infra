resource "yandex_compute_instance" "vm-1" {
	name				= var.name
	folder_id			= var.folder_id
	platform_id			= var.platform_id
	zone				= var.zone
	resources {
		cores			= var.resources_core
		memory			= var.resources_memory
	}
	scheduling_policy {
		preemptible		= var.scheduling_policy
	}
	boot_disk {
		mode = "READ_WRITE"
		initialize_params {
			image_id	= var.boot_disk_image_id
			size		= var.boot_disk_size
			}
		}
	network_interface {
		subnet_id		= var.subnet_id
		nat				= true
		}
	metadata = {
		user-data		= "${file("cloud-config.yaml")}"
		}
}
