Модуль для инстанса ВМ
Используются переменные, задаются в ./terraform/modules/tf-yc-instance/variables.tf:

	variables "name" {
		default           = "chapter5-lesson2-std-021-011"
		description       = "Instance name. MUST BE CHANGE "

	variable "platform_id" {} 
		default           = "standard-v1"
		description       = "Instance standard platforms"

	variable "resources_core" {}
		default     = 2 
		description = "Number of CPU"

	variable "resources_memory" {}
		default     = 2 
		description = "Instance RAM size (GB)"

	variable "scheduling_policy" {} 
		default     = "true"
		description = "Preemptible instance "

	variable "boot_disk_size" {}
		default     = 30
		description = "Instance HDD size"

	variable "boot_disk_image_id" {}
		default     = "fd80qm01ah03dkqb14lc"
		description = "Image ID for instance system"

Используются переменные, задаются в ./terraform/modules/tf-yc-instancevariables.tf:

	variable "cloud_id" {}
		description = "Cloud ID, находится в terraform.tfvars"
		cloud_id	= "b1g3jddf4nv5e9okle7p"
	variable "folder_id" {}
		description = "Folder ID, находится в terraform.tfvars"
		folder_id	= "b1g3nvv8sngeob717ei7"
	variable "zone" {}
		default           = "ru-central1-a"
		description       = "Instance availability zone"

Генерируется автоматически: в main.tf 

	variable "subnet_id" {}
		lookup(module.tf-yc-network.network_zones, var.zone)

Используется провайдер:

    source  = "yandex-cloud/yandex"
    version = "=0.99.1"

Выводится:

	output "ip_address_external" {
    	value		= yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
		description = "Внешний ip адрес VM"

	output "subnet_id_result" {
		value		= var.subnet_id
		description = "Subnet Id у VM для проверки (добавлено для тестирования lookup(module.tf-yc-network.network_zones, var.zone))"
