variable "name" {
    type              = string
    default           = "chapter5-lesson2-std-021-011"
    description       = "Instance name"
}
variable "platform_id" {
  type              = string
  default           = "standard-v1"
  description       = "Instance standard platforms"
  validation {
    condition       = contains(toset(["standard-v1", "standard-v2", "standard-v3","gpu-standard-v1", "gpu-standard-v2", "gpu-standard-v3", "gpu-standard-v3-t4"]), var.platform_id)
    error_message   = "Instance standard platforms: standard-v1...v3, gpu-standard-v1...v3, standard-v3-t4 ."
	}
  nullable = false
}
variable "resources_core" {
  default     = 2 
  type        = number
  description = "Number of CPU"
  nullable = false 
}
variable "resources_memory" {
  default     = 2 
  type        = number
  description = "Instance RAM size (GB)"
  nullable = false 
}
variable "scheduling_policy" {
  default     = "true"
  type        = bool
  description = "Preemptible instance "
  nullable = false 
} 
variable "boot_disk_size" {
  default     = 30 
  type        = number
  description = "Instance HDD size"
  nullable = false 
}
variable "boot_disk_image_id" {
  default     = "fd80qm01ah03dkqb14lc"
  type        = string
  description = "Image ID for instance system"
  nullable = false 
}
variable "subnet_id" {}
variable "cloud_id" {}
variable "folder_id" {}
variable "zone" {}