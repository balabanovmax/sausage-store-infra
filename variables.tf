variable "zone" {  
default           = "ru-central1-a"
  type              = string
  description       = "Instance availability zone"
  validation {
    condition       = contains(toset(["ru-central1-a", "ru-central1-b", "ru-central1-c"]), var.zone)
    error_message   = "Availability zone: ru-central1-a, ru-central1-b, ru-central1-c."
	}
  nullable = false
}
variable "folder_id" {
    type              = string
    default           = "b1g3nvv8sngeob717ei7"
    description       = "YC Folder ID"
    }
variable "cloud_id" {
    type              = string
    default           = "b1g3jddf4nv5e9okle7p"
    description       = "YC Cloud ID"
    }

