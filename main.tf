terraform {
  required_version = "= 1.5.7" 
}
module "tf-yc-instance" {
	source					= "./modules/tf-yc-instance"
	zone					= var.zone
	folder_id				= var.folder_id
	cloud_id				= var.cloud_id
	subnet_id				= lookup(module.tf-yc-network.network_zones, var.zone)
}
module "tf-yc-network" {
	source					= "./modules/tf-yc-network"
	subnet_id				= lookup(module.tf-yc-network.network_zones, var.zone)
}
