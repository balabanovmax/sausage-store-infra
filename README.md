В конфигураци представлены два модуля для instance и network

При масштабировании обязательно изменить имя в var.name модуля instance


    variables "name" {
    type              = string
    default           = "chapter5-lesson2-std-021-011"
    }

Зона по умолчанию, изменяется в ./variables

    variable "zone" {
    default           = "ru-central1-a"
    }

Версия провайдеров зафиксирована на 0.99.1

Все переменные для настройки ВМ находятся в

    modules/tf-yc-instance/variables.tf

Продробнее описано в 

    modules/tf-yc-instance/readme.tf
