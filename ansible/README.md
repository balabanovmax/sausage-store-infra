# Ansible Playbook для развертывания сервиса сосисочной на подготовленной через Terraform VM

# ansible.cfg
- [defaults]
- roles_path = roles #путь к каталогу ролей
- inventory = inventory #путь к каталогу серверов
- remote_user = ansible # user по-умолчанию для подключения по ssh
- private_key_file = /home/student/.ssh/id_rsa #ключ для подключения по ssh 
- ask_vault_pass = True #запрашивать пароль для зашифрованных данных vault
- host_key_checking = False #отключение проверки ключа хоста
- [persistent_connection] 
- command_timeout = 30 #таймаут подключения

# group_vars: 
  Переменные для проекта, выделены группы frontend.yaml и backend.yaml и общая группа all.yaml
- backend_maven_version: 1.0.845961
- backend_service_user: jarservice
- backend_report_directory: /opt/sausage-store/reports
- backend_lib_directory: /opt/sausage-store/backend/lib/
- password: Зашифровано с ansible-vault, указывается пароль для подключения к Nexus. Можно указать пароль в явном виде, либо зашифровать через ansible-vault encrypt_string --encrypt-vault-id 
#
- frontend_service_user: www-data
- frontend_version: 1.0.845960
- frontend_folder: /var/www-data/
- password: Зашифровано с ansible-vault, указывается пароль для подключения к Nexus. Можно указать пароль в явном виде, либо зашифровать через ansible-vault encrypt_string --encrypt-vault-id 
# inventory: 

- yandexcloud.yaml 

Описываются имена/ip адреса серверов для подключения
##

# roles/
Разделение tasks под ролям


# roles/backend/tasks/main.yaml

Описаны tasks для установки зависимостей, установка сервиса backend на VM

 - name: Update and upgrade apt packages
 - name: Install PIP
 - name: Install lxml
 - name: create dir
 - name: wget openjdk
 - name: unzip openjdk
 - name: maven_artifact download nexus
 - name: add user backend
 - name: chmod download backend file 
 - name: add service backend
 - name: reload systemd
 - name: run service
# roles/frontend/tasks/main.yaml
Описаны tasks для установки зависимостей, установка сервиса frontend на VM

 - name: Install the package npm
 - name: Install nginx
 - name: add user frontend
 - name: create dir frontend & chmod
 - name: Download file from nexus
 - name: unzip download frontend & copy to www-data
 - name: add nginx config
 - name: reload nginx

# templates

Шаблоны для генерации backend и fronted сервисов на VM
# sausage-store-backend.service.j2

Конфиг для сервиса java приложения

# sausage-store.conf
Конфиг для nginx


